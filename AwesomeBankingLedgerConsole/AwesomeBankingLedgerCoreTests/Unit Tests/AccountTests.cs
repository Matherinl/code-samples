﻿using AwesomeBankingLedgerCore.Data_Interface;
using AwesomeBankingLedgerCore.Database_Context;
using AwesomeBankingLedgerCore.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace AwesomeBankingLedgerCoreTests.Unit_Tests {
	[TestClass]
	public class AccountTests {
		[TestCleanup]
		public void TestCleanup() {
			DbContext.ListAccounts.Clear();
		}

		[TestMethod]
		public void TestAccountsForUser_ShouldHaveTwoAccountsForUser2() {
			User user1=UserLogic.CreateUser("chewbacca","password!");
			User user2=UserLogic.CreateUser("Obiwan","theforceisgreat123");
			AccountLogic.CreateAccount("Savings",user1.UserNum);
			AccountLogic.CreateAccount("Checking",user2.UserNum);
			AccountLogic.CreateAccount("MoneyForLuke",user2.UserNum);
			Assert.IsTrue(AccountLogic.GetAccountsForUser(user2.UserNum).Count==2);
		}

		[TestMethod]
		public void TestIsAccountNameValid_ShouldNotCreateAccount() {
			User user=UserLogic.CreateUser("chewbacca","password!");
			AccountLogic.CreateAccount("Savings",user.UserNum);
			Assert.IsFalse(AccountLogic.IsAccountNameValid("Savings",user));
		}

		[TestMethod]
		public void TestAccountUpdate_ShouldUpdateAccountBalance() {
			User user=UserLogic.CreateUser("chewbacca","password!");
			Account acc=AccountLogic.CreateAccount("Savings",user.UserNum);
			acc.AccountBalance=100;
			Assert.IsTrue(DbContext.ListAccounts.Find(x=>x.AccountNum==acc.AccountNum).AccountBalance==100);
		}
	}
}
