using AwesomeBankingLedgerCore.Data_Interface;
using AwesomeBankingLedgerCore.Database_Context;
using AwesomeBankingLedgerCore.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AwesomeBankingLedgerCoreTests.Unit_Tests {
	[TestClass]
	public class UserTests {

		[TestCleanup]
		public void TestCleanup() {
			DbContext.ListUsers.Clear();
		}

		///<summary>User entered the correct password for the user.</summary>
		[TestMethod]
		public void TestPasswordHashing_ValidatePasswordShouldReturnTrue() {
			User user=UserLogic.CreateUser("LukeSkywalker","Ouchmyhand!23");
			Assert.IsTrue(UserLogic.IsPasswordMatch("Ouchmyhand!23",user));
		}

		///<summary>User entered incorrect password for the user.</summary>
		[TestMethod]
		public void TestPasswordHashing_ValidatePasswordShouldReturnFalse() {
			User user=UserLogic.CreateUser("LukeSkywalker","Ouchmyhand!23");
			Assert.IsFalse(UserLogic.IsPasswordMatch("Ouchmyhand123",user));
		}

		///<summary>User entered a unique username.</summary>
		[TestMethod]
		public void TestUniqueUserName_UserShouldBeCreated() {
			User user=UserLogic.CreateUser("LukeSkywalker","Ouchmyhand!23");
			Assert.AreEqual(user,UserLogic.GetUserByName("LukeSkywalker"));
		}

		///<summary>User entered a username already in use.</summary>
		[TestMethod]
		public void TestDuplicateUserName_UserShouldExist() {
			UserLogic.CreateUser("LukeSkywalker","Ouchmyhand!23");
			User userDuplicate=null;
			if(UserLogic.IsUserNameValid("LukeSkywalker")) {
				userDuplicate=UserLogic.CreateUser("LukeSkywalker","blah123");
			}
			Assert.IsTrue(userDuplicate==null);
		}

		///<summary>User has logged in.</summary>
		[TestMethod]
		public void TestUserLoggedIn_UserIsLoggedInShouldReturnTrue() {
			User user=UserLogic.CreateUser("LukeSkywalker","Ouchmyhand!23");
			UserLogic.LoginUser(user);
			Assert.IsTrue(user.IsLoggedIn);
		}

		///<summary>User has logged out.</summary>
		[TestMethod]
		public void TestUserLoggedIn_UserIsLoggedInShouldReturnFalse() {
			User user=UserLogic.CreateUser("LukeSkywalker","Ouchmyhand!23");
			UserLogic.LogoutUser(user);
			Assert.IsFalse(user.IsLoggedIn);
		}

		///<summary>Tries to create a user with a username that already exists.</summary>
		[TestMethod]
		public void TestIsUserNameValid_UserShouldNotBeCreated() {
			User user=UserLogic.CreateUser("LukeSkywalker","Ouchmyhand!23");
			Assert.IsFalse(UserLogic.IsUserNameValid("LukeSkywalker"));
		}

		///<summary>Tries to get a user with a username that does not exist.</summary>
		[TestMethod]
		public void TestGetUserByName_ShouldReturnNull() {
			User user=UserLogic.CreateUser("LukeSkywalker","Ouchmyhand!23");
			Assert.IsTrue(UserLogic.GetUserByName("BenSolo")==null);
		}
	}
}
