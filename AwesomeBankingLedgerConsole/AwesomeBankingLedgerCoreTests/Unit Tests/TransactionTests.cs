﻿using AwesomeBankingLedgerCore.Database_Context;
using AwesomeBankingLedgerCore.Models;
using AwesomeBankingLedgerCore.Data_Interface;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace AwesomeBankingLedgerCoreTests.Unit_Tests {
	[TestClass]
	public class TransactionTests {
		[TestCleanup]
		public void TestCleanup() {
			DbContext.ListTransactions.Clear();
			DbContext.ListAccounts.Clear();
		}

		[TestMethod]
		public void TestMakeDeposit_ShouldHaveAccountBalanceOf100() {
			User user=UserLogic.CreateUser("HanSolo","ilovechewy");
			Account accChecking=AccountLogic.CreateAccount("Checking",user.UserNum);
			Transaction t=TransactionLogic.MakeDeposit(accChecking,100);
			Assert.IsTrue(DbContext.ListAccounts[0].AccountBalance==100);
		}

		[TestMethod]
		public void TestMakeWithDrawl_ShouldHaveAccountBalanceOf50() {
			User user=UserLogic.CreateUser("HanSolo","ilovechewy");
			Account accSavings=AccountLogic.CreateAccount("Savings",user.UserNum);
			TransactionLogic.MakeDeposit(accSavings,100);
			TransactionLogic.MakeWithdrawl(accSavings,50);
			Assert.IsTrue(DbContext.ListAccounts[0].AccountBalance==50);
		}

		[TestMethod]
		public void TestCanMakeWithdrawl_ShouldNotMakeWithdrawl() {
			User user=UserLogic.CreateUser("HanSolo","ilovechewy");
			Account accSavings=AccountLogic.CreateAccount("Savings",user.UserNum);
			TransactionLogic.MakeDeposit(accSavings,100);
			Assert.IsFalse(TransactionLogic.CanMakeWithdrawl(101,accSavings));
		}
	}
}
