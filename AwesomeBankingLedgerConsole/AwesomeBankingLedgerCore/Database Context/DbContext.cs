﻿using AwesomeBankingLedgerCore.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AwesomeBankingLedgerCore.Database_Context {
	/// <summary>Represents our "database" object. There is no persistant storage per the project description. For simplicity's sake I made this class static.</summary>
	public static class DbContext {
		public static List<User> ListUsers=new List<User>();
		public static List<Account> ListAccounts=new List<Account>();
		public static List<Transaction> ListTransactions=new List<Transaction>();
	}
}
