﻿using AwesomeBankingLedgerCore.Database_Context;
using AwesomeBankingLedgerCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AwesomeBankingLedgerCore.Data_Interface {
	public class AccountLogic {
		///<summary>The next primary key value available.</summary>
		private static long AccountNum=0;
		///<summary>Manually handles incrementing the primary key since we are not using a database.</summary>
		public static long GetAccountNum {
			get {
				long retVal=AccountNum;
				AccountNum++;
				return retVal;
			}
		}

		public static Account CreateAccount(string accountName,long userNum) {
			Account accountNew=new Account(GetAccountNum,accountName,userNum);
			accountNew.AccountNum=GetAccountNum;
			accountNew.DateTimeCreated=DateTime.Now;
			DbContext.ListAccounts.Add(accountNew);
			return accountNew;
		}

		///<summary>Gets a list of accounts for the given userNum.</summary>
		public static List<Account> GetAccountsForUser(long userNum) {
			return DbContext.ListAccounts.Where(x=>x.UserNum==userNum).ToList();
		}

		///<summary>Gets an account object for the given user with the specified name.</summary>
		public static Account GetAccountByName(long userNum,string accountName) {
			return DbContext.ListAccounts.Find(x=>x.UserNum==userNum && x.AccountName==accountName);
		}

		///<summary>Checks to see if any account for the given user has the specified name.</summary>
		public static bool DoesAccountExist(string accountName,long userNum){ 
			return GetAccountsForUser(userNum).Exists(x=>x.AccountName==accountName);	
		}

		///<summary>Check to see if the new account name already exists in the list of accounts. If it does, returns false as we don't allow duplicate account names.
		///If it doesn't, returns true. Should generally be called before CreateAccount().</summary>
		public static bool IsAccountNameValid(string accountNameNew,List<Account> listAccountsForUser) {
			if(listAccountsForUser.Exists(x => x.AccountName==accountNameNew)) {
				return false;
			}
			return true;
		}

		///<summary>Check to see if the new account name already exists for the user. If it does, returns false as we don't allow duplicate account names.
		///If it doesn't, returns true. Should generally be called before CreateAccount().</summary>
		public static bool IsAccountNameValid(string accountNameNew,User user) {
			List<Account> listAccountsForUser=AccountLogic.GetAccountsForUser(user.UserNum);
			if(listAccountsForUser.Exists(x => x.AccountName==accountNameNew)) {
				return false;
			}
			return true;
		}
	}
}
