﻿using AwesomeBankingLedgerCore.Database_Context;
using AwesomeBankingLedgerCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace AwesomeBankingLedgerCore.Data_Interface {
	public class UserLogic {
		///<summary>The next primary key value available.</summary>
		private static long UserNum=0;
		///<summary>Manually handles incrementing the primary key since we are not using a database.</summary>
		public static long GetUserNum {
			get {
				long retVal=UserNum;
				UserNum++;
				return retVal;
			}
		}

		///<summary>Returns the user object with the given username. Can return null if the userName does not exist.</summary>
		public static User GetUserByName(string userName) {
			return DbContext.ListUsers.FirstOrDefault(x=>x.UserName==userName);
		}

		///<summary>Creates a user with the given userName and password. The password is hashed before adding to our "database".</summary>
		public static User CreateUser(string userName,string plainTextPass) {
			string passHash=HashPassword(plainTextPass);
			User userNew=new User(GetUserNum,userName,passHash);
			DbContext.ListUsers.Add(userNew);
			return userNew;
		}

		///<summary>Given a plain text password, this method will return a password hash. This should always be called before storing a user's password.</summary>
		private static string HashPassword(string plainTextPassword) {
			byte[] salt=new byte[16];
			byte[] hash;
			byte[] hashBytes=new byte[36];
			new RNGCryptoServiceProvider().GetBytes(salt);
			Rfc2898DeriveBytes rfcBytes=new Rfc2898DeriveBytes(plainTextPassword,salt,1000);
			hash=rfcBytes.GetBytes(20);
			Array.Copy(salt,0,hashBytes,0,16);
			Array.Copy(hash,0,hashBytes,16,20);
			return Convert.ToBase64String(hashBytes);
		}

		///<summary>Validates the given user's password hash against the entered password. Returns true if a match, false otherwise.</summary>
		public static bool IsPasswordMatch(string inputPassword,User user) {
			byte[] hashBytes=Convert.FromBase64String(user.PasswordHash);
			byte[] salt= new byte[16];
			Array.Copy(hashBytes,0,salt,0,16);
			/*Compute the hash for the entered password*/
			Rfc2898DeriveBytes rfcBytes=new Rfc2898DeriveBytes(inputPassword,salt,1000);
			byte[] hash=rfcBytes.GetBytes(20);
			for(int i = 0;i<20;i++) {
				if(hashBytes[i+16]!=hash[i]) {
					return false;
				}
			}
			return true;
		}

		///<summary>Finds the given user in the provided DbContext and logs them in</summary>
		public static void LoginUser(User user) {
			DbContext.ListUsers.Find(x=>x.UserNum==user.UserNum).IsLoggedIn=true;
		}

		///<summary>Finds the given user in the provided DbContext and logs them in</summary>
		public static void LogoutUser(User user) {
			DbContext.ListUsers.Find(x=>x.UserNum==user.UserNum).IsLoggedIn=false;
		}

		///<summary>Determines if the provided username already exists. We cannot allow duplicate usernames. Returns true if valid, false otherwise.
		///Should generally be called before CreateUser().</summary>
		public static bool IsUserNameValid(string userName) {
			if(DbContext.ListUsers.Exists(x => x.UserName==userName)) {
				return false;
			}
			return true;
		}
	}
}
