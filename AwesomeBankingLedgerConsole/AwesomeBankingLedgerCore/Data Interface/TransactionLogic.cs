﻿using System;
using System.Collections.Generic;
using System.Text;
using AwesomeBankingLedgerCore.Database_Context;
using AwesomeBankingLedgerCore.Models;

namespace AwesomeBankingLedgerCore.Data_Interface {
	public class TransactionLogic {		
		///<summary>The next primary key value available.</summary>
		private static long TransactionNum=0;
		///<summary>Manually handles incrementing the primary key since we are not using a database.</summary>
		public static long GetTransactionNum {
			get {
				long retVal=TransactionNum;
				TransactionNum++;
				return retVal;
			}
		}

		///<summary>Creates a transaction object for the given amt (can be negative) and account.</summary>
		public static Transaction MakeDeposit(Account account,double amt) {
			Transaction t=new Transaction(amt,DateTime.Now,account.AccountNum,Transaction.TransactionType.Deposit);
			t.TransactionNum=GetTransactionNum;
			DbContext.ListTransactions.Add(t);
			//Update the account balance
			account.AccountBalance+=t.TransactionAmt;
			return t;
		}

		///<summary>Creates a transaction object for the given amt (can be negative) and account.</summary>
		public static Transaction MakeWithdrawl(Account account,double amt) {
			Transaction t=new Transaction(amt,DateTime.Now,account.AccountNum,Transaction.TransactionType.Withdrawl);
			t.TransactionNum=GetTransactionNum;
			DbContext.ListTransactions.Add(t);
			//Update the account balance
			account.AccountBalance-=t.TransactionAmt;
			return t;
		}

		///<summary>Get a list of transactions for the given account.</summary>
		public static List<Transaction> GetTransactionsForAccount(long accountNum) {
			return DbContext.ListTransactions.FindAll(x=>x.AccountNum==accountNum);
		}

		///<summary>Determines whether the given account has enough money to make a withdrawl for the specified amount.</summary>
		public static bool CanMakeWithdrawl(double amt,Account account) {
			if(account.AccountBalance<amt) {
				return false;
			}
			return true;
		}
	}
}
