﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AwesomeBankingLedgerCore.Models {
	/// <summary>
	/// This class represents a single transaction. A transaction is linked to an account since a single account can have many transactions.
	/// </summary>
	public class Transaction {
		///<summary>Primary key</summary>
		public long TransactionNum;
		///<summary>The amount the transaction was for. Will be negative for withdrawls.</summary>
		public double TransactionAmt;
		///<summary>The date and time when the transaction occurred.</summary>
		public DateTime TransactionDateTime;
		///<summary>The account the transaction is for.</summary>
		public long AccountNum;
		public TransactionType TranType;

		public Transaction(double amt,DateTime tranDate,long accountNum,TransactionType type) {
			TransactionAmt=amt;
			TransactionDateTime=tranDate;
			AccountNum=accountNum;
			TranType=type;
		}

		///<summary>The type of transaction that was made.</summary>
		public enum TransactionType {
			///<summary>0</summary>
			Deposit,
			///<summary>1</summary>
			Withdrawl,
		}
	}
}
