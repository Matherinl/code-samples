﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AwesomeBankingLedgerCore.Models {
	/// <summary>
	/// Represents a single bank account. Each account is linked to a particular user. This allows a single user to have many accounts (checking, savings, retirement, etc).
	/// </summary>
	public class Account {
		///<summary>Primary key</summary>
		public long AccountNum;
		public string AccountName;
		///<summary>The current balance for the account.</summary>
		public double AccountBalance;
		///<summary>FK to User. This account is linked to this user.</summary>
		public long UserNum;
		///<summary>Set programatically on account creation.</summary>
		public DateTime DateTimeCreated;

		public Account(long accountNum,string accountName,long userNum) {
			AccountName=accountName;
			AccountBalance=0;
			UserNum=userNum;
			DateTimeCreated=DateTime.Now;
		}
	}
}
