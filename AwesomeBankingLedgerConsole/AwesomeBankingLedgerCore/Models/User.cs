﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AwesomeBankingLedgerCore.Models {
	/// <summary>
	/// Represents a single user. The password is always stored as a hash for security purposes. See Users.cs for password hashing algorithm.
	/// </summary>
	public class User {
		///<summary>Primary key</summary>
		public long UserNum;
		public string UserName;
		public string PasswordHash;
		public bool IsLoggedIn;
		///<summary>Not currently used, but nice information to have in the future.</summary>
		public DateTime LastLoggedInAt;

		public User(long userNum,string userName,string passHash) {
			UserNum=userNum;
			UserName=userName;
			PasswordHash=passHash;
		}
	}
}
