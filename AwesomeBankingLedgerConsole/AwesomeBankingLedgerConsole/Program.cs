﻿using AwesomeBankingLedgerCore.Database_Context;
using AwesomeBankingLedgerCore.Data_Interface;
using AwesomeBankingLedgerCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AwesomeBankingLedgerConsole {
	public class Program {
		///<summary>The user that is currently logged in. Null until a successful login.</summary>
		private static User CurUser;

		static void Main(string[] args) {
			bool endApp=false;
			CurUser=null;
			Console.WriteLine("--------------------------------------------WORLD'S GREATEST BANKING LEDGER---------------------------------------------");
			while(!endApp) {
				//No users exist, prompt the user to make one.
				while(DbContext.ListUsers.Count==0) {
					CreateFirstUserPrompt();
				}
				//At least one user exists, prompt them to log in.
				while(CurUser==null) {
					CurUser=UserLoginPrompt();
				}
				while(CurUser!=null && CurUser.IsLoggedIn) {
					string input=Console.ReadLine();
					string[] arrayInputs=input.Split(" ");
					string command=arrayInputs[0];
					string param1=GetParam1(arrayInputs);
					string param2=GetParam2(arrayInputs);
					switch(command) {
						case "mkaccount":
							MakeAccount(param1);
							break;
						case "listaccounts":
							ListAccounts();
							break;
						case "deposit":
							MakeDeposit(param1,param2);
							break;
						case "withdraw":
							MakeWithdrawl(param1,param2);
							break;
						case "showbal":
							ShowAccountBalance(param1);
							break;
						case "transhist":
							ShowTransactionHist(param1);
							break;
						case "logout":
							UserLogic.LogoutUser(CurUser);
							CurUser=null;
							break;
						case "exit":
							endApp=true;
							return;
						case "commands":
							ListUserCommands();
							break;
						default:
							Console.WriteLine("Unrecognized command. For a list of commands type: commands");
							break;
					}
				}
			}
		}

		///<summary>Contains the logic for prompting the user of the console app to create a user.</summary>
		private static void CreateFirstUserPrompt() {
			Console.WriteLine("There are currently no users. Would you like to create one? (Y/N)");
			string response=Console.ReadKey().KeyChar.ToString();
			if(response!="y" && response!="Y") {
				WriteNewLine("You cannot use the banking ledger without first creating a user.");
			}
			else {//User hit 'y' or 'Y'
				CreateUserPrompt();
			}
		}

		private static void CreateUserPrompt() {
			WriteNewLine("Please enter a username. It must be at least 4 characters long.");
			string userName=Console.ReadLine();
			while(!IsUserNameInputValid(userName)) {
				WriteNewLine("Invalid username: The username must be 4 characters long, cannot contain spaces, and must contain 1 letter. Please try again.");
				userName=Console.ReadLine();
			}
			Console.WriteLine("Please enter a password. It must be at least 8 characters long.");
			string password;
			MaskPasswordInput(out password);
			while(password.Length<8 || String.IsNullOrWhiteSpace(password)) {
				WriteNewLine("Invalid password: The password must be at least 8 characters long.");
				MaskPasswordInput(out password);
			}
			UserLogic.CreateUser(userName,password);
			WriteNewLine("User created successfully!");
		}

		///<summary>Contains the logic for logging a user in.</summary>
		private static User UserLoginPrompt() {
			WriteNewLine("To log in please enter your username.");
			string userName=Console.ReadLine();
			User user=UserLogic.GetUserByName(userName);
			if(user==null) {
				Console.WriteLine("No user with that name exists. Would you like to creat a new user? (Y/N)");
				string response=Console.ReadKey().KeyChar.ToString();
				if(response=="y" || response=="Y") {
					CreateUserPrompt();
					return UserLoginPrompt();
				}
				else {
					//The user either hit 'N' or an invalid command. Either way start the login prompt over again.
					UserLoginPrompt();
				}
			}
			else {//Entered an existing username
				WriteNewLine("Please enter your password");
				string password;
				MaskPasswordInput(out password);
				while(!UserLogic.IsPasswordMatch(password,user)) {
					WriteNewLine("Password entered is incorrect. Please try again.");
					MaskPasswordInput(out password);
				}
				//User has entered the correct password
				UserLogic.LoginUser(user);
				WriteNewLine("Login successful!");
				ListUserCommands();
				return user;
			}
			//Should never get here. The user must either make a user and login or login to an existing user before continuing.
			return null;
		}

		#region Commands
		///<summary>Creates an account for CurUser with the given account name, if valid.</summary>
		private static void MakeAccount(string accountName) {
			if(!AccountLogic.IsAccountNameValid(accountName,CurUser)) {
				Console.WriteLine($"An account named '{accountName}' already exists.");
				return;
			}
			else if(string.IsNullOrWhiteSpace(accountName)) {
				Console.WriteLine("Invalid account name.");
				return;
			}
			//Valid account name
			AccountLogic.CreateAccount(accountName,CurUser.UserNum);
			Console.WriteLine($"{accountName} created");
		}

		///<summary>Lists the accounts for the currently logged in user.</summary>
		private static void ListAccounts() {
			List<Account> listAccs=AccountLogic.GetAccountsForUser(CurUser.UserNum);
			if(listAccs.Count==0) {
				Console.WriteLine("You currently have no accounts.");
			}
			else {
				Console.WriteLine(String.Join(",",listAccs.Select(x=>x.AccountName)));
			}
		}

		private static void MakeDeposit(string amt,string accountName) {
			double amount;
			try {
				amount=Convert.ToDouble(amt);
			}
			catch(Exception) {
				Console.WriteLine("Could not convert amount to a valid number. Please try again.");
				return;
			}
			//Don't allow negative deposits.
			if(amount<0) {
				Console.WriteLine("A deposit cannot be negative.");
				return;
			}
			if(!AccountLogic.DoesAccountExist(accountName,CurUser.UserNum)) {
				Console.WriteLine($"No account named {accountName} found.");
				return;
			}
			Account account=AccountLogic.GetAccountByName(CurUser.UserNum,accountName);
			TransactionLogic.MakeDeposit(account,amount);
			Console.WriteLine($"{account.AccountName} balance: ${account.AccountBalance}");
		}

		private static void MakeWithdrawl(string amt,string accountName) {
			double amount;
			try {
				amount=Convert.ToDouble(amt);
			}
			catch(Exception) {
				Console.WriteLine("Could not convert amount to a valid number. Please try again.");
				return;
			}
			//Don't allow negative withdrawls.
			if(amount<0) {
				Console.WriteLine("You cannot make a negative withdrawl.");
				return;
			}
			if(!AccountLogic.DoesAccountExist(accountName,CurUser.UserNum)) {
				Console.WriteLine($"No account named {accountName} found.");
				return;
			}
			Account account=AccountLogic.GetAccountByName(CurUser.UserNum,accountName);
			if(!TransactionLogic.CanMakeWithdrawl(amount,account)) {
				Console.WriteLine("Insufficient funds. Cannot complete transaction.");
				return;
			}
			TransactionLogic.MakeWithdrawl(account,amount);
			Console.WriteLine($"{account.AccountName} balance: ${account.AccountBalance}");
		}

		private static void ShowAccountBalance(string accountName) {
			if(!AccountLogic.DoesAccountExist(accountName,CurUser.UserNum)) {
				Console.WriteLine($"No account named {accountName} found.");
				return;
			}
			Console.WriteLine($"${AccountLogic.GetAccountByName(CurUser.UserNum,accountName).AccountBalance}");
		}

		private static void ShowTransactionHist(string accountName) {
			Account acc=AccountLogic.GetAccountByName(CurUser.UserNum,accountName);
			if(acc==null) {
				Console.WriteLine($"Account named {accountName} not found.");
				return;
			}
			List<Transaction> listTrans=TransactionLogic.GetTransactionsForAccount(acc.AccountNum);
			foreach(Transaction tran in listTrans) {
				Console.WriteLine($"{tran.TranType}: ${tran.TransactionAmt},{tran.TransactionDateTime}");
			}
		}

		///<summary>Writes a list of commands and examples available to the user once logged in to the console.</summary>
		private static void ListUserCommands() {
			Console.WriteLine("While logged in you can use the following commands:");
			Console.WriteLine("--------------------------------------------------------COMMANDS--------------------------------------------------------");
			Console.WriteLine("'mkaccount NAME' - Creates an account with the specified name, e.g., mkaccount Savings");
			Console.WriteLine("'listaccounts' - Lists the accounts for the user, e.g., listaccounts");
			Console.WriteLine("'deposit AMOUNT ACCOUNTNAME' - Deposits the specified amount into the account, e.g., deposit 500 Savings");
			Console.WriteLine("'withdraw AMOUNT ACCOUNTNAME' - Withdraws the specified amount from the account, e.g., withdraw 500 Savings");
			Console.WriteLine("'showbal ACCOUNTNAME' - Lists the balance of the given account, e.g., showbal Savings");
			Console.WriteLine("'transhist ACCOUNTNAME' - Lists all transactions for the given account name, e.g., transhist Savings");
			Console.WriteLine("'logout' - Logs the current user out");
			Console.WriteLine("'exit' - Closes the application");
			Console.WriteLine("------------------------------------------------------------------------------------------------------------------------");
		}
		#endregion

		#region Helper Methods
				
		///<summary>Helper method that removes the need for sprinkling \r\n after reading user input. This prevents Console.WriteLine() from
		///writing to the same line the user input is on after Console.ReadKey() is called.</summary>
		private static void WriteNewLine(string input) {
			Console.WriteLine($"\r\n{input}");
		}

		///<summary>A user name is valid if it is 4 characters or longer, does not contain a space, has at least 1 letter, and is not a duplicate of an existing username.</summary>
		private static bool IsUserNameInputValid(string input) {
			bool hasOneLetter=false;
			foreach(char c in input.ToCharArray()) {
				if(char.IsLetter(c)) {
					hasOneLetter=true;
					break;
				}
			}
			if(input.Length<4 
				|| input.Contains(" ") 
				|| !hasOneLetter 
				|| String.IsNullOrWhiteSpace(input)
				|| !UserLogic.IsUserNameValid(input)) 
			{
				return false;
			}
			return true;
		}

		///<summary>Masks the user's input by replacing the characters typed with '*'. Outs the actual password string.</summary>
		private static void MaskPasswordInput(out string password) {
			bool isTyping=true;
			password="";
			while(isTyping) {
				ConsoleKeyInfo key=Console.ReadKey(true);
				if(key.Key!=ConsoleKey.Backspace && key.Key!=ConsoleKey.Enter) {
					string typedChar=key.KeyChar.ToString();
					password+=typedChar;
					Console.Write("*");
				}
				else if(key.Key==ConsoleKey.Backspace) {
					if(password.Length!=0){
						password=password.Remove(password.Length-1);
						Console.Write("\b \b");
					};
				}
				else{//User hit 'Enter'
					isTyping=false;
				}
			}
		}

		///<summary>Get's the first parameter from a user command. If not specified, returns "".</summary>
		private static string GetParam1(string[] arrayInputs) {
			if(arrayInputs.Length<2) {
				return "";
			}
			else {
				return arrayInputs[1];
			}
		}

		///<summary>Get's the second parameter from a user command. If not specified, returns "".</summary>
		private static string GetParam2(string[] arrayInputs) {
			if(arrayInputs.Length<3) {
				return "";
			}
			else {
				return arrayInputs[2];
			}
		}
		#endregion
	}
}
